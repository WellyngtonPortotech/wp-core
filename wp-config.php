<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'wordpress' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'HX.kw]97zk}V]J}n@P*O|A0M2f`[Uv2L@a/8faaQW|dli$i-8wtmI8G59`J5^QT0' );
define( 'SECURE_AUTH_KEY',  '93)*jJ{u{noh0mP[y8!-Sp_9IILV}xY@2@)XFQ,dW+IG75g&XVto=n2;X%7^_RNQ' );
define( 'LOGGED_IN_KEY',    'ULYG2R|9{1NBi|~;{WRI}4sg+9aL5+,Z-+xG~}PUX|2n)vhL<TmB51B~a,f1D~pY' );
define( 'NONCE_KEY',        '4`m$/z),koISfcPW?+$G5w8pIwvtBO2VhEhpY]u@~/`4@w(38sO#T<=>iesghb<K' );
define( 'AUTH_SALT',        '^n$Sd0vh0J{tRT3D+Z:.%_@dB#|.DUQmYi;UqOi}-@P?ZW`>caSjjMQwH@{QcwH<' );
define( 'SECURE_AUTH_SALT', 'zLX <io^(oH1ch2f5U`g]{g> :j.?MU%Aq~n&>,WgNI_.<M6[^|]VbLfHJFrv>x,' );
define( 'LOGGED_IN_SALT',   '!ArU T?OqGu:4_2Wc<CK8]tr?Pa@]f Q~nKY*B$+_f4*$gi5zPD54G<Qr(T}AEJT' );
define( 'NONCE_SALT',       'q|S&30P1% P70gERrM}`k,m,{vP5_G#6%CitCX@R{.xcQ/(eU&/&6x`uWZ2uB!{D' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
